﻿namespace JoyWorld {
    partial class Form1 {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent() {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numMaxVelocity = new System.Windows.Forms.NumericUpDown();
            this.numMinVelocity = new System.Windows.Forms.NumericUpDown();
            this.numMaxRadius = new System.Windows.Forms.NumericUpDown();
            this.numMinRadius = new System.Windows.Forms.NumericUpDown();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.numBallCount = new System.Windows.Forms.NumericUpDown();
            this.pictureBox1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxVelocity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinVelocity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBallCount)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.numMaxVelocity);
            this.panel1.Controls.Add(this.numMinVelocity);
            this.panel1.Controls.Add(this.numMaxRadius);
            this.panel1.Controls.Add(this.numMinRadius);
            this.panel1.Controls.Add(this.btnGenerate);
            this.panel1.Controls.Add(this.numBallCount);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(687, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(231, 674);
            this.panel1.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 12);
            this.label5.TabIndex = 10;
            this.label5.Text = "Max Velocity";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "Min Velocity";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "Max Radius";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "Min Radius";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ball Count";
            // 
            // numMaxVelocity
            // 
            this.numMaxVelocity.Location = new System.Drawing.Point(89, 119);
            this.numMaxVelocity.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numMaxVelocity.Name = "numMaxVelocity";
            this.numMaxVelocity.Size = new System.Drawing.Size(120, 21);
            this.numMaxVelocity.TabIndex = 5;
            this.numMaxVelocity.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numMinVelocity
            // 
            this.numMinVelocity.Location = new System.Drawing.Point(89, 92);
            this.numMinVelocity.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numMinVelocity.Name = "numMinVelocity";
            this.numMinVelocity.Size = new System.Drawing.Size(120, 21);
            this.numMinVelocity.TabIndex = 4;
            this.numMinVelocity.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // numMaxRadius
            // 
            this.numMaxRadius.Location = new System.Drawing.Point(89, 65);
            this.numMaxRadius.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numMaxRadius.Name = "numMaxRadius";
            this.numMaxRadius.Size = new System.Drawing.Size(120, 21);
            this.numMaxRadius.TabIndex = 3;
            this.numMaxRadius.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // numMinRadius
            // 
            this.numMinRadius.Location = new System.Drawing.Point(89, 38);
            this.numMinRadius.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numMinRadius.Name = "numMinRadius";
            this.numMinRadius.Size = new System.Drawing.Size(120, 21);
            this.numMinRadius.TabIndex = 2;
            this.numMinRadius.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(89, 146);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(120, 23);
            this.btnGenerate.TabIndex = 1;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            // 
            // numBallCount
            // 
            this.numBallCount.Location = new System.Drawing.Point(89, 11);
            this.numBallCount.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numBallCount.Name = "numBallCount";
            this.numBallCount.Size = new System.Drawing.Size(120, 21);
            this.numBallCount.TabIndex = 0;
            this.numBallCount.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Yellow;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(687, 674);
            this.pictureBox1.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(918, 674);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Game loop";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxVelocity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinVelocity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBallCount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown numBallCount;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numMaxVelocity;
        private System.Windows.Forms.NumericUpDown numMinVelocity;
        private System.Windows.Forms.NumericUpDown numMaxRadius;
        private System.Windows.Forms.NumericUpDown numMinRadius;
        private System.Windows.Forms.Panel pictureBox1;
    }
}

