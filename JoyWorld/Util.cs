﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;
using System.Reflection;

namespace JoyWorld {
    internal class Util {
        internal static double GetTime() {
            return (double)Stopwatch.GetTimestamp() / Stopwatch.Frequency;
        }

        internal static void Swap(ref int x1, ref int x2) {
            int temp = x1;
            x1 = x2;
            x2 = temp;
        }
    }

    internal static class Extensions {
        public static void SetDoubleBuffered(this Control control, bool value) {
            var propertyInfo = typeof(Control).GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic);
            propertyInfo.SetValue(control, value, null);
        }
    }
}
