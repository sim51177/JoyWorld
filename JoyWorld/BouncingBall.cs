﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JoyWorld {
    class BouncingBall {
        public static Size WindowSize { get; set; }
        public static Random Random { get; set; }

        public PointF pos;
        public PointF radius;
        public PointF vel;
        public Color color;
        public bool ellipseOrRect;

        public BouncingBall(int minRadius, int maxRadius, int minVelocity, int maxVelocity) {
            if (Random == null)
                Random = new Random();

            pos = new PointF(Random.Next(WindowSize.Width), Random.Next(WindowSize.Height));
            radius = new PointF(Random.Next(minRadius, maxRadius), Random.Next(minRadius, maxRadius));
            var velAbs = Random.Next(minVelocity, maxVelocity);
            var velAngle = Random.NextDouble() * 2 * Math.PI;
            vel = new PointF((float)(velAbs * Math.Cos(velAngle)), (float)(velAbs * Math.Sin(velAngle)));
            color = Color.FromArgb(Random.Next(256), Random.Next(256), Random.Next(256));
            ellipseOrRect = Random.Next(2) == 0;
        }

        public void UpdateTime(float timeDelta) {
            pos.X += vel.X * timeDelta;
            pos.Y += vel.Y * timeDelta;

            if (pos.X < radius.X) {
                pos.X = radius.X;
                vel.X = -vel.X;
            }
            if (pos.X > WindowSize.Width - radius.X) {
                pos.X = WindowSize.Width - radius.X;
                vel.X = -vel.X;
            }
            if (pos.Y < radius.Y) {
                pos.Y = radius.Y;
                vel.Y = -vel.Y;
            }
            if (pos.Y > WindowSize.Height - radius.Y) {
                pos.Y = WindowSize.Height - radius.Y;
                vel.Y = -vel.Y;
            }
        }

        internal void Draw(DibSection dib) {
            var brBorder = Color.Blue.ToArgb();
            bool fill = true;
            if (ellipseOrRect) {
                int cx = (int)pos.X;
                int cy = (int)pos.Y;
                int rx = (int)radius.X;
                int ry = (int)radius.Y;
                int smallRx = (int)radius.X - 4;
                int smallRy = (int)radius.Y - 4;
                Drawing.DrawEllipse(dib.BufPtr, dib.Width, dib.Height, cx, cy, rx, ry, brBorder, fill);
                Drawing.DrawEllipse(dib.BufPtr, dib.Width, dib.Height, cx, cy, smallRx, smallRy, color.ToArgb(), fill);
            }
            else {
                int x1 = (int)(pos.X - radius.X);
                int y1 = (int)(pos.Y - radius.Y);
                int x2 = (int)(pos.X + radius.X);
                int y2 = (int)(pos.Y + radius.Y);

                int smallX1 = (int)(pos.X - radius.X + 4);
                int smallY1 = (int)(pos.Y - radius.Y + 4);
                int smallX2 = (int)(pos.X + radius.X - 4);
                int smallY2 = (int)(pos.Y + radius.Y - 4);
                Drawing.DrawRectangle(dib.BufPtr, dib.Width, dib.Height, x1, y1, x2, y2, brBorder, fill);
                Drawing.DrawRectangle(dib.BufPtr, dib.Width, dib.Height, smallX1, smallY1, smallX2, smallY2, color.ToArgb(), fill);
            }
        }
    }
}
