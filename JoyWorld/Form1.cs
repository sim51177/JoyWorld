﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Input;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Drawing.Drawing2D;

namespace JoyWorld {
    public partial class Form1 : Form {
        private double timeOld;
        private string keyInfo;
        private string fpsInfo;
        private float fpsRefreshTime = 0;
        private DibSection dib;

        // game contents
        private List<BouncingBall> balls = new List<BouncingBall>();

        public Form1() {
            InitializeComponent();
            ControlSetStyle(pictureBox1, ControlStyles.Opaque, true);
            //pictureBox1.SetDoubleBuffered(true);        // 더블 버퍼링 활성화

            Shown += Form1_Shown;                       // 폼이 보여질 때
            pictureBox1.Layout += pictureBox1_Layout;  // 레이아웃 함수
            btnGenerate.Click += btnGenerate_Click;    // 버튼 클릭 함수
        }

        private void ControlSetStyle(Control control, ControlStyles styles, bool enable) {
            Type type = control.GetType();
            MethodInfo method = type.GetMethod("SetStyle", BindingFlags.NonPublic | BindingFlags.Instance);
            method.Invoke(control, new object[] { styles, enable });
        }

        
        private void Form1_Shown(object sender, EventArgs e) {
            InitGame();

            timeOld = Util.GetTime();        // 최초 시간 측정
            while (Created) {
                Application.DoEvents();     // 메시지 처리

                double timeNow = Util.GetTime(); // 시간 측정
                float timeDelta = (float)(timeNow - timeOld);   // 경과시간 계산

                ProcessInput();             // 입력 처리
                UpdateFrame(timeDelta);     // 프레임 갱신
                Render();                   // 화면 드로우

                timeOld = timeNow;          // 측정 시간 보관
                //Thread.Sleep(1);                 // CPU 휴식 
            }

            FreeGame();
        }

        // 초기화
        private void InitGame() {
            RegenBuffer();
            GenerateBalls();
        }

        // 정리
        private void FreeGame() {
        }

        // 입력 처리
        private void ProcessInput() {
            keyInfo = string.Empty;
            if (Keyboard.IsKeyDown(Key.W))
                keyInfo += "W ";
            if (Keyboard.IsKeyDown(Key.S))
                keyInfo += "S ";
            if (Keyboard.IsKeyDown(Key.A))
                keyInfo += "A ";
            if (Keyboard.IsKeyDown(Key.D))
                keyInfo += "D ";
        }

        // 프레임 갱신
        private void UpdateFrame(float timeDelta) {
            fpsRefreshTime += timeDelta;
            if (fpsRefreshTime >= 0.1) {
                float fps = 1.0f / timeDelta;
                fpsInfo = string.Format("{0:0000.0} (FPS)", fps);
                fpsRefreshTime = 0;
            }

            balls.ForEach(ball => ball.UpdateTime(timeDelta));
        }

        // 렌더
        private void Render() {
            Render(dib);               // 버퍼에 그리기
            dib.BitBlt();
        }

        // 버퍼 재생성
        private void RegenBuffer() {
            BouncingBall.WindowSize = pictureBox1.ClientSize;
            dib = new DibSection(pictureBox1.Handle, BouncingBall.WindowSize.Width, BouncingBall.WindowSize.Height);
        }

        // 볼 생성
        private void GenerateBalls() {
            int ballNum = (int)numBallCount.Value;
            int minRadius = (int)numMinRadius.Value;
            int maxRadius = (int)numMaxRadius.Value;
            int minVelocity = (int)numMinVelocity.Value;
            int maxVelocity = (int)numMaxVelocity.Value;

            balls = Enumerable.Range(0, ballNum).Select(i => new BouncingBall(minRadius, maxRadius, minVelocity, maxVelocity)).ToList();
        }

        private void btnGenerate_Click(object sender, EventArgs e) {
            GenerateBalls();
        }

        private void pictureBox1_Layout(object sender, LayoutEventArgs e) {
            RegenBuffer();
        }

        private void Render(DibSection dib) {
            Drawing.DrawRectangle(dib.BufPtr, dib.Width, dib.Height, 0, 0, dib.Width, dib.Height, Color.White.ToArgb(), true);
            // draw black grid lines every 100 pixels, horizontal and vertical
            for (int x = 0; x < BouncingBall.WindowSize.Width; x += 50) {
                Drawing.DrawLine(dib.BufPtr, dib.Width, dib.Height, x, 0, x, BouncingBall.WindowSize.Height, Color.Black.ToArgb());
            }
            for (int y = 0; y < BouncingBall.WindowSize.Height; y += 50) {
                Drawing.DrawLine(dib.BufPtr, dib.Width, dib.Height, 0, y, BouncingBall.WindowSize.Width, y, Color.Black.ToArgb());
            }

            balls.ForEach(ball => ball.Draw(dib));
            using (var g = Graphics.FromHdc(dib.Hdc)) {
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.DrawString(fpsInfo, Font, Brushes.Black, 0, 4);
                g.DrawString(keyInfo, Font, Brushes.Black, 0, 20);
            }
        }
    }
}